<?php

namespace Tests\Unit;

use Source\Reconciliation\Transaction\Merchant;
use Source\Reconciliation\Transaction\Place;
use Tests\TestCase;

/**
 * Class PlaceCorrectTest
 * @package Tests\Unit
 */
class PlaceCorrectTest extends TestCase
{

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCorrectDate()
    {
        $price = new Place(new Merchant('empty merchant', null, null), '2014-01-12 10:09:46');
        $actualDate = $this->invokeMethod($price, 'tryCorrectDate', ['2014-01-12 14:09:36']);
        $this->assertTrue($actualDate === '2014-01-12 10:09:46');

        $price = new Place(new Merchant('empty merchant', null, null), '2014-01-12 14:09:36');
        $actualDate = $this->invokeMethod($price, 'tryCorrectDate', ['2014-01-12 10:09:46']);
        $this->assertTrue($actualDate === '2014-01-12 14:09:36');
    }

    public function testIncorrectDates()
    {
        $price = new Place(new Merchant('empty merchant', null, null), '2013-01-12 14:09:46');
        $actualDate = $this->invokeMethod($price, 'tryCorrectDate', ['2014-01-12 14:09:36']);
        $this->assertTrue($actualDate === '2014-01-12 14:09:36');

    }

    /**
     * Call protected/private method of a class.
     *
     * @param object &$object Instantiated object that we will run method on.
     * @param string $methodName Method name to call
     * @param array $parameters Array of parameters to pass into method.
     *
     * @return mixed Method return.
     */
    public function invokeMethod(&$object, $methodName, array $parameters = [])
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
}
