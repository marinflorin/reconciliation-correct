<?php

namespace Tests\Unit;

use Source\Reconciliation\Transaction\Price;
use Tests\TestCase;

/**
 * Class PriceCorrectTest
 * @package Tests\Unit
 */
class PriceCorrectTest extends TestCase
{

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCorrectPrices()
    {
        $price = new Price(19770, 0, 'DEDUCT');
        $actualPrice = $this->invokeMethod($price, 'tryCorrectAmount', [39405]);
        $this->assertTrue($actualPrice === 39405);


        $price = new Price(39405, 0, 'DEDUCT');
        $actualPrice = $this->invokeMethod($price, 'tryCorrectAmount', [19770]);
        $this->assertTrue($actualPrice === 19770);
    }

    public function testIncorrectPrices()
    {
        $price = new Price(39405, 0, 'DEDUCT');
        $actualPrice = $this->invokeMethod($price, 'tryCorrectAmount', [39405.1]);
        $this->assertTrue($actualPrice === 39405);
    }

    /**
     * Call protected/private method of a class.
     *
     * @param object &$object Instantiated object that we will run method on.
     * @param string $methodName Method name to call
     * @param array $parameters Array of parameters to pass into method.
     *
     * @return mixed Method return.
     */
    public function invokeMethod(&$object, $methodName, array $parameters = [])
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
}
