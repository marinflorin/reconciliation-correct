<?php

namespace Source\Contracts\Repository;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class AbstractRepository
 * @package Source\Contracts\Repository
 */
class AbstractRepository
{
    /** @var Model $model */
    protected $model;

    /**
     * Return all items
     *
     * @return Collection
     */
    public function findAll()
    {
        return $this->model->all();
    }

    /**
     * Return a model based on PK id
     *
     * @param int $id
     * @param bool $failIfNotFound
     * @return \Illuminate\Database\Eloquent\Collection|Model|null|static|static[]
     */
    public function find($id, $failIfNotFound = false)
    {
        if ($failIfNotFound) {
            return $this->model->findOrFail($id);
        }

        return $this->model->find($id);
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete($id)
    {
        $model = $this->model->findOrFail($id);
        return $model->delete();
    }
}
