<?php

namespace Source\Reconciliation\Contracts;

/**
 * Interface TransactionMatchInterface
 * @package Source\Reconciliation\Contracts
 */
interface TransactionMatchInterface
{
    public function match(TransactionMatchInterface $transaction, bool $correct = true);
}
