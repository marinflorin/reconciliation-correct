<?php

namespace Source\Reconciliation\Factories;

use Source\Reconciliation\Exceptions\Merchant\MissingCountryCodeException;
use Source\Reconciliation\Exceptions\Merchant\MissingCountryException;
use Source\Reconciliation\Transaction\Merchant;

/**
 * Class MerchantFactory
 * @package Source\Reconciliation\Factories
 */
class MerchantFactory
{
    /**
     * Extract the country and the city from merchant name
     *
     * @param string $merchantData
     * @return Merchant
     */
    public static function make($merchantData): Merchant
    {
        try {
            $countryCode = self::extractCountryCode($merchantData);
            $country = self::extractCountry($merchantData, $countryCode);
        } catch (MissingCountryCodeException $exception) {
            $countryCode = '';
            $country = '';
        } catch (MissingCountryException $exception) {
            $country = '';
        } finally {
            /** @var string $countryCode */
            $merchantName = self::extractMerchantName($merchantData, $countryCode, $country);
        }

        return new Merchant($merchantName, $country = null, $countryCode = null);
    }

    /**
     * Extract country from merchant name
     *
     * @param string $merchantData
     * @param string $countryCode
     * @return string
     * @throws MissingCountryException
     */
    private static function extractCountry($merchantData, string $countryCode): string
    {
        try {
            return trim(str_replace($countryCode, '', explode('   ', $merchantData, 2)[1]));
        } catch (\Exception $exception) {
            throw new MissingCountryException($exception->getMessage());
        }

    }

    /**
     * Extract country code from merchant name
     *
     * @param string $merchantData
     * @return string countryCode
     * @throws MissingCountryCodeException
     */
    private static function extractCountryCode($merchantData)
    {
        try {
            return trim(explode('      ', $merchantData, 2)[1]);
        } catch (\Exception $exception) {
            throw new MissingCountryCodeException($exception->getMessage());
        }
    }

    /**
     * Extract merchant name by removing other data
     *
     * @param string $merchantData
     * @param string $countryCode
     * @param string $country
     *
     * @return string merchantName
     */
    private static function extractMerchantName($merchantData, $countryCode, $country)
    {
        return trim(str_replace([$countryCode, $country], '', $merchantData));
    }
}
