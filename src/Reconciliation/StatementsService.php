<?php

namespace Source\Reconciliation;

use Source\Reconciliation\Transaction\Transaction;

/**
 * Class StatementsService
 * @package Source\Reconciliation
 */
class StatementsService
{
    /** @var TransactionMapper $transactionMapper */
    private $transactionMapper;

    /** @var SuggestStatementService $suggestStatementService */
    private $suggestStatementService;

    CONST CSV_DELIMITER = ',';

    /**
     * StatementsService constructor.
     * @param TransactionMapper $transactionMapper
     * @param SuggestStatementService $suggestStatementService
     */
    public function __construct(TransactionMapper $transactionMapper, SuggestStatementService $suggestStatementService)
    {
        $this->transactionMapper = $transactionMapper;
        $this->suggestStatementService = $suggestStatementService;
    }

    /**
     * Build the transaction object from csv line then try to correct the transactions
     *
     * @param array $client
     * @param array $tutuka
     * @return array
     */
    public function matchStatements(array $client, array $tutuka)
    {
        $clientStatements = $this->parseStatements($client);
        $tutukaStatements = $this->parseStatements($tutuka);

        return $this->suggestStatementService->correctStatements($clientStatements, $tutukaStatements);
    }

    /**
     * Parse the csv lines and map one line to a transaction object
     *
     * @param array $client
     * @return array
     */
    private function parseStatements(array $client): array
    {
        /** @var Transaction[] $statements */
        $statements = [];
        foreach ($client as $line) {

            /** @var array $bankStatement */
            $bankStatement = $this->breakLine($line);
            $statements[] = $this->transactionMapper->mapRowToTransaction($bankStatement);
        }

        return $statements;
    }

    /**
     * Create an array from the csv line, later to be mapped to an object
     *
     * @param string $line
     * @return array
     */
    private function breakLine(string $line): array
    {
        return explode(
        // Remove new line character
            self::CSV_DELIMITER, trim(preg_replace('/\s\s+/', ' ', $line))
        );
    }
}
