<?php

namespace Source\Reconciliation;

use Illuminate\Support\Collection;
use Source\Reconciliation\Exceptions\Transaction\TransactionNotFound;
use Source\Reconciliation\Transaction\Transaction;

/**
 * Class SuggestStatementService
 * @package Source\Reconciliation
 */
class SuggestStatementService
{
    /**
     * Match transaction based on transaction id and try to correct the transactions from two collections
     *
     * @param array $client
     * @param array $tutuka
     * @return array
     */
    public function correctStatements(array $client, array $tutuka)
    {

        $client = $this->buildCollection($client);
        $tutuka = $this->buildCollection($tutuka);

        return $this->matchStatements($client, $tutuka);
    }

    /**
     * @todo maybe use an object instead
     * @param array $bankStatements
     * @return Collection
     */
    private function buildCollection(array &$bankStatements)
    {
        $arrayToCollection = [];
        foreach ($bankStatements as $transaction) {
            /** @var Transaction $transaction */
            $arrayToCollection[$transaction->getId()] = $transaction;
        }

        return new Collection($arrayToCollection);
    }


    /**
     *
     *
     * @param Collection $client
     * @param Collection $tutuka
     * @return array
     */
    private function matchStatements(Collection $client, Collection $tutuka): array
    {
        /** @var Transaction $transaction */
        $nr = 0;
        foreach ($client as $key => $transaction) {
            try {

                // Try to correct a transaction with tutuka transaction based on transactionId
                if ($transaction->match($tutuka->get($key))) {
                    $nr++;
                }
            } catch (TransactionNotFound $exception) {
                // we do not have a matching transaction for the provided transaction id
                // do nothing
            }
        }

        return [
            'match'  => $nr,
            'client' => $client,
            'tutuka' => $tutuka,
        ];
    }
}