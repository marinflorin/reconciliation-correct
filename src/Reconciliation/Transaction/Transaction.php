<?php

namespace Source\Reconciliation\Transaction;

use Source\Reconciliation\Exceptions\Transaction\TransactionNotFound;

/**
 * Class Transaction
 * @package Source\Reconciliation\Transaction
 */
class Transaction implements \JsonSerializable
{
    /** @var Place $place */
    private $place;

    /** @var Price $price */
    private $price;

    /** @var string $profileName */
    private $profileName;

    /** @var Meta $meta */
    private $meta;

    /** @var  Transaction $suggestion */
    private $dirty;

    /**
     * Transaction constructor.
     * @param Place $place
     * @param Price $price
     * @param Meta $meta
     * @param $profileName
     */
    public function __construct(Place $place, Price $price, Meta $meta, $profileName)
    {
        $this->place = $place;
        $this->price = $price;
        $this->meta = $meta;
        $this->profileName = $profileName;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->meta->getId();
    }

    /**
     * @param Transaction $transaction
     * @param bool $correct
     * @return bool
     * @throws TransactionNotFound
     */
    public function match(Transaction $transaction = null, bool $correct = true)
    {
        if ($transaction === null) {
            throw new TransactionNotFound();
        }

        $equalTransactions = $this == $transaction;;
        if ($correct && !$equalTransactions) {
            // save initial state of objects
            $thisDirty = json_encode($this);
            $transactionDirty = json_encode($transaction);

            $this->place->match($transaction->place);
            $this->price->match($transaction->price);
            $this->meta->match($transaction->meta);


            $equalTransactions = $this == $transaction;
            // initial object state is different and after correction they match
            if ($thisDirty !== $transactionDirty && $this == $transaction) {
                $this->dirty = $thisDirty;
                $transaction->dirty = $transactionDirty;
            }

        }

        return $equalTransactions;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {

        return [
            'place'       => $this->place,
            'price'       => $this->price,
            'profileName' => $this->profileName,
            'meta'        => $this->meta,
            'dirty'       => json_decode($this->dirty, true), // This is already a json, we dont want to encode it again
        ];
    }
}
