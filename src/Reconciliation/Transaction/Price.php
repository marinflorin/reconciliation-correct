<?php

namespace Source\Reconciliation\Transaction;

/**
 * Class Price
 * @package Source\Reconciliation\Transaction
 */
class Price implements \JsonSerializable
{
    /** @var float $amount */
    private $amount;

    /** @var string $type */
    private $type;

    /** @var string $description */
    private $description;

    CONST MAX_PRICE_DIFFERENCE = 1;

    /**
     * TransactionPrice constructor.
     * @param float $amount
     * @param integer $type
     * @param string $description
     */
    public function __construct($amount, $type, $description)
    {
        $this->amount = abs($amount);
        $this->type = $type;
        $this->description = $description;
    }

    /**
     * Check if the prices are equal, try to correct the amount
     *
     * @param Price $price
     * @param bool $correct
     * @return bool
     */
    public function match(Price $price, bool $correct = true)
    {
        if ($correct && $this != $price) {
            if ($this->amount === $price->amount && $this->type === $price->type) {
                $price->description = $this->description;
            } elseif ($this->type === $price->type && $this->description === $price->description) {
                $price->amount = $this->tryCorrectAmount($price->amount);
            }
        }

        return $this == $price;
    }

    /**
     * Return the correct amount
     *
     * @param float $amount
     * @return float
     */
    private function tryCorrectAmount($amount)
    {
        if (abs(abs($this->amount) - abs($amount)) < self::MAX_PRICE_DIFFERENCE) {
            return $this->amount;
        }

        return $amount;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        return [
            'amount'      => $this->amount,
            'type'        => $this->type,
            'description' => $this->description,
        ];
    }
}
