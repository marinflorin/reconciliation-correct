<?php

namespace Source\Reconciliation\Transaction;

/**
 * Class Merchant
 * @package Source\Reconciliation\Transaction
 */
class Merchant implements \JsonSerializable
{
    /** @var string $name */
    protected $name;

    /** @var string $city */
    protected $country;

    /** @var string $countryCode */
    protected $countryCode;

    CONST SIMILAR_PERCENTAGE_ACCEPTABLE_DIFFERENCE = 50;

    /**
     * Merchant constructor.
     *
     * @param string $name
     * @param string $country
     * @param string $countryCode
     */
    public function __construct($name, $country, $countryCode)
    {
        $this->name = $name;
        $this->country = $country;
        $this->countryCode = $countryCode;
    }

    /**
     * @todo check if it breaks SRP
     * @todo would be better if we leave this as an immutable object-value?
     *
     * Check if two merchants are equal, try to correct one mistake.
     *
     * @param Merchant $merchant
     * @param bool $correct
     * @return bool
     */
    public function match(Merchant $merchant, bool $correct = true)
    {
        if ($correct && $this != $merchant) {
            if ($this->countryCode === $merchant->countryCode && $this->country === $merchant->country) {
                similar_text($this->name, $merchant->name, $similarPercentage);

                if ($similarPercentage > self::SIMILAR_PERCENTAGE_ACCEPTABLE_DIFFERENCE) {
                    $merchant->name = $this->name;
                }
            } elseif ($this->name === $merchant->name && $this->country === $merchant->country) {
                $merchant->countryCode = $this->countryCode;
            } elseif ($this->name === $merchant->name && $this->countryCode === $merchant->countryCode) {
                $merchant->country = $this->country;
            }
        }

        return $this == $merchant;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        return [
            'country'     => $this->country,
            'countryCode' => $this->countryCode,
            'name'        => $this->name,
        ];
    }
}
