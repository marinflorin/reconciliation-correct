<?php

namespace Source\Reconciliation\Transaction;

/**
 * Class Place
 * @package Source\Reconciliation\Transaction
 */
class Place implements \JsonSerializable
{
    /** @var Merchant $merchant */
    private $merchant;

    /** @var string $date */
    private $date;

    CONST MAX_DATES_DIFFERENCE_IN_DAYS = 2;

    /**
     * Place constructor.
     * @param Merchant $merchant
     * @param $date
     */
    public function __construct(Merchant $merchant, $date)
    {
        $this->merchant = $merchant;
        $this->date = $date;
    }

    /**
     * Check if two places are equal, try to correct the merchant and the date
     *
     * @param Place $place
     * @param bool $correct
     * @return bool
     */
    public function match(Place $place, bool $correct = true)
    {
        if ($correct && $this != $place) {
            $this->merchant->match($place->merchant);
            if ($this->date !== $place->date) {
                $place->date = $this->tryCorrectDate($place->date);
            }
        }

        return $this == $place;
    }

    /**
     * Try to correct a date if the difference between two dates of two transactions is acceptable
     *
     * @param string $date
     * @return string
     */
    private function tryCorrectDate(string $date)
    {
        $dateDiff = abs((strtotime($this->date) - strtotime($date)) / (60 * 60 * 24));
        if ($dateDiff < self::MAX_DATES_DIFFERENCE_IN_DAYS) {
            return $this->date;
        }

        return $date;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'date'     => $this->date,
            'merchant' => $this->merchant,
        ];
    }
}
