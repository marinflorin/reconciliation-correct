<?php

namespace Source\Reconciliation\Transaction;

/**
 * Class Meta
 * @package Source\Reconciliation\Transaction
 */
class Meta implements \JsonSerializable
{
    /** @var int $id */
    private $id;

    /** @var string $walletReference */
    private $walletReference;

    /**
     * Meta constructor.
     * @param integer $id
     * @param string $walletReference
     */
    public function __construct($id, $walletReference)
    {
        $this->id = $id;
        $this->walletReference = $walletReference;
    }

    /**
     * Check if two metas are equal, try to correct missing wallet information
     *
     * @param Meta $meta
     * @param bool $correct
     * @return bool
     */
    public function match(Meta $meta, bool $correct = true)
    {
        if ($correct && $this != $meta) {
            // Fix missing wallet info
            if (!$this->walletReference) {
                $this->walletReference = $meta->walletReference;
            } elseif (!$meta->walletReference) {
                $meta->walletReference = $this->walletReference;
            }

        }

        return $this == $meta;
    }

    /**
     * Get transaction Id
     *
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        return [
            'id'              => $this->id,
            'walletReference' => $this->walletReference,
        ];
    }
}
