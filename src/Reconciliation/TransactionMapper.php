<?php

namespace Source\Reconciliation;

use Source\Reconciliation\Factories\MerchantFactory;
use Source\Reconciliation\Transaction\Meta;
use Source\Reconciliation\Transaction\Place;
use Source\Reconciliation\Transaction\Price;
use Source\Reconciliation\Transaction\Transaction;

/**
 * Class TransactionMapper
 * @package Source\Reconciliation
 */
class TransactionMapper
{
    /**
     * Map the csv line to objects
     *
     * @param array $data
     * @return Transaction
     */
    public function mapRowToTransaction(array $data): Transaction
    {
        return new Transaction(
            new Place(
                MerchantFactory::make($data[3]), $data[1]
            ),
            new Price($data[2], $data[6], $data[4]),
            new Meta($data[5], $data[7]),
            $data[0]
        );
    }
}
