<?php

namespace Source\Reconciliation\Exceptions\Merchant;

/**
 * Class MissingCountryCodeException
 * @package Source\Reconciliation\Exceptions\Merchant
 */
class MissingCountryCodeException extends \Exception
{

}
