<?php

namespace Source\Reconciliation\Exceptions\Merchant;

/**
 * Class MissingCountryException
 * @package Source\Reconciliation\Exceptions\Merchant
 */
class MissingCountryException extends \Exception
{

}
