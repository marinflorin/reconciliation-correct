<?php

namespace Source\Reconciliation\Exceptions\Transaction;

/**
 * Class TransactionNotFound
 * @package Source\Reconciliation\Exceptions\Transaction
 */
class TransactionNotFound extends \Exception
{

}
