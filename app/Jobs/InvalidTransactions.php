<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Source\Reconciliation\StatementsService;
use Source\Report\ReportRepository;

/**
 * Class InvalidTransactions
 * @package App\Jobs
 */
class InvalidTransactions implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var  string $data */
    private $data;

    /** @var $statementsService StatementsService */
    private $statementsService;

    /**
     * Get unmatched transactions and try to correct them
     *
     * @param StatementsService $statementsService
     * @param ReportRepository $reportRepository
     * @return void
     */
    public function handle(StatementsService $statementsService, ReportRepository $reportRepository)
    {
        $data = json_decode($this->data);
        $this->statementsService = $statementsService;

        $reportRepository->create('client_report', json_encode(
            $this->statementsService->matchStatements($data->client, $data->tutuka)
        ));
    }
}
