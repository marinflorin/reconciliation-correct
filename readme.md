## Correcting Transaction ##

####Important!: The only field we dont correct is the transaction ID.
We dont correct the transaction id because it could represent a threat from outside for our system. EX:

1. We validate their transaction id and match our transaction id to theirs, if we do this they can trade a 1$ transaction for a 1000$ transaction.
2. We change their transaction id to match ours, if we do this we can validate an invalid transaction


### Communication between services ###

The communication between services is done with queues. This allows us a background processing of large files and also data reliability. 

### Correction service ###

We try to correct transactions with the following algorithm:

1. Similar text for merchants
2. Close amounts, the amounts may differ from conversions this is the reason we check numbers for a small difference
3. Missing wallet info
4. Close dates

Send the corrections to the main service, we send the correction even if in the end transactions are still different.

### Improvements ###

1. Use a response object instead of array of jsons of transactions, at this moment we are exposing more then we should and we bind a class hierarchy for transaction object.